package com.ghostapps.movies

import com.ghostapps.movies.utils.DateHelper
import org.junit.Test

import org.junit.Assert.*
import java.util.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class UnitTest {

    var dateHelper = DateHelper()

    @Test
    fun getFormattedDateFromDateHelper() {
        val date = Date(1533265200000) // 03/08/2017
        val formattedDate = dateHelper.getFormatedDate(date)

        assertEquals("2018-08-03", formattedDate)
    }

    @Test
    fun addDaysFromDateHelper() {
        var date = Date(1533265200000) // 03/08/2017
        date = dateHelper.addDays(date, 6)
        val formattedDate = dateHelper.getFormatedDate(date)

        assertEquals("2018-08-09", formattedDate)
    }


}
