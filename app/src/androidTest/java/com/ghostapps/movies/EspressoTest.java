package com.ghostapps.movies;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.ghostapps.movies.R;
import com.ghostapps.movies.modules.MainActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class EspressoTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void espressoTestHome() {
        waitFor(2000);

        /* After movies list is showed, choose first element */
        onView(allOf(withId(R.id.moviesList),
                childAtPosition(
                        withClassName(is("android.support.constraint.ConstraintLayout")),
                        2)))
                .perform(actionOnItemAtPosition(0, click()));

        /* Check if some movie is showed */
        onView(withId(R.id.movieDetailTitle)).check(matches(isDisplayed()));

        /* Check if some movie is showed */
        onView(withId(R.id.movieDetailTitle)).check(matches(isDisplayed()));
    }

    @Test
    public void espressoTestSearch() {
        /* Click on search icon */
        onView(withId(R.id.movieListToolbarSearchIcon)).perform(click());

        /* Write "lion king" */
        onView(withId(R.id.movieSearchToolbarField)).perform(replaceText("lion king"));

        /* Search icon click */
        onView(withId(R.id.movieSearchToolbarSearchIcon)).perform(click());

        /* wait list loading */
        waitFor(2000);

        /* Chose first movie */
        ViewInteraction recyclerView2 = onView(
                allOf(withId(R.id.moviesList),
                        childAtPosition(
                                withClassName(is("android.support.constraint.ConstraintLayout")),
                                3)));
        recyclerView2.perform(actionOnItemAtPosition(0, click()));

        /* Check if some movie is showed */
        onView(withId(R.id.movieDetailTitle)).check(matches(isDisplayed()));
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }

    private void waitFor(long timeInMillis) {
        try {
            Thread.sleep(timeInMillis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
