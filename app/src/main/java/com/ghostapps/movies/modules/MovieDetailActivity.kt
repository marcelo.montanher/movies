package com.ghostapps.movies.modules

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.ghostapps.movies.R
import com.ghostapps.movies.models.MovieModel
import com.ghostapps.movies.services.PicassoService
import kotlinx.android.synthetic.main.activity_movie_detail.*

class MovieDetailActivity: AppCompatActivity() {

    companion object {
        val MOVIE_DETAILS = "movie_details"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)

        setupView()
    }

    private fun setupView() {
        movieDetailToolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        val movie = intent.getParcelableExtra<MovieModel>(MOVIE_DETAILS)

        val picassoService = PicassoService(this)
        picassoService.loadImage(movie.backdrop_path, movieDetailImage, {
            movieDetailImageLoading.visibility = View.GONE
        }, {})

        movieDetailTitle.text = movie.title
        movieDetailRating.text = movie.vote_average.toString()
        movieDetailVoteCount.text = movie.vote_count.toString()
        movieDetailReleaseDate.text = getString(R.string.movie_detail_release_date, movie.release_date)
        movieDetailOriginalLanguage.text = getString(R.string.movie_detail_original_language, movie.original_language)
        movieDetailDescription.text = movie.overview
    }

}