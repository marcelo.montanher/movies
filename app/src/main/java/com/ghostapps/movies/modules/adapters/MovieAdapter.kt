package com.ghostapps.movies.modules.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ghostapps.movies.R
import com.ghostapps.movies.models.MovieModel
import com.ghostapps.movies.services.PicassoService
import kotlinx.android.synthetic.main.cell_movie.view.*

class MoviesListAdapter(private val movies: ArrayList<MovieModel>?,
                        private val context: Context?,
                        private val listener: MoviesListAdapterListener): RecyclerView.Adapter<MoviesListAdapter.ViewHolder>() {

    interface MoviesListAdapterListener {
        fun onMovieClicked(movie: MovieModel?)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.cell_movie, parent, false)
        return ViewHolder(view, listener, context)
    }

    override fun getItemCount(): Int {
        return movies?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(movies?.get(position))
    }

    class ViewHolder(itemView: View, val listener: MoviesListAdapterListener, val context: Context?) : RecyclerView.ViewHolder(itemView) {
        fun bindView(movie: MovieModel?) {
            val picassoService = PicassoService(context)

            itemView.movieCardImageLoading.visibility = View.VISIBLE

            picassoService.loadImage(movie?.backdrop_path, itemView.movieCardImage, {
                itemView.movieCardImageLoading.visibility = View.GONE
            }, {})
            itemView.movieCardTitle.text = movie?.title
            itemView.movieCardDescription.text = movie?.overview
            itemView.movieCardRating.text = movie?.vote_average.toString()

            itemView.setOnClickListener {
                listener.onMovieClicked(movie)
            }
        }
    }


}