package com.ghostapps.movies.modules

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.ghostapps.movies.R
import com.ghostapps.movies.models.MovieModel
import com.ghostapps.movies.modules.adapters.MoviesListAdapter
import com.ghostapps.movies.services.MoviesService
import com.ghostapps.movies.utils.LoadingHelper

import kotlinx.android.synthetic.main.activity_movie_search.*

class MovieSearchActivity : AppCompatActivity(), MoviesListAdapter.MoviesListAdapterListener {

    private var moviesService: MoviesService? = null
    private var loadingHelper: LoadingHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_search)

        moviesService = MoviesService(this)
        loadingHelper = LoadingHelper(this)

        setupButtons()
    }

    override fun onMovieClicked(movie: MovieModel?) {
        val intent = Intent(this, MovieDetailActivity::class.java)
        intent.putExtra(MovieDetailActivity.MOVIE_DETAILS, movie)
        startActivity(intent)
    }

    private fun setupButtons() {
        movieSearchToolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        movieSearchToolbarSearchIcon.setOnClickListener {
            doSearch()
        }
    }

    private fun doSearch() {
        val query = movieSearchToolbarField.text.toString()
        loadingHelper?.showProgress()

        moviesService?.subscribe(query,
                {
                    moviesList.adapter = MoviesListAdapter(it, this,this)
                    val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
                    moviesList.layoutManager = layoutManager
                    loadingHelper?.hideProgress()
                },{})
    }

}
