package com.ghostapps.movies.utils

import android.content.Context
import android.preference.PreferenceManager
import com.ghostapps.movies.models.MovieModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


class CacheHelper(val context: Context) {

    companion object {
        const val HOME_LIST_TAG = "homeList"
    }

    val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    val editor = sharedPreferences.edit()
    val gson = Gson()

    fun getStoredHomeList(): ArrayList<MovieModel>? {
        val listAsJson = sharedPreferences.getString(HOME_LIST_TAG, "")
        val type = TypeToken.getParameterized(ArrayList::class.java, MovieModel::class.java).type

        if (listAsJson.isNullOrEmpty()) {
            return null
        } else {
            return gson.fromJson(listAsJson, type) as ArrayList<MovieModel>
        }
    }

    fun saveHomeList(list: ArrayList<MovieModel>) {
        val listAsJson = gson.toJson(list)

        editor.putString(HOME_LIST_TAG, listAsJson)
        editor.commit()
    }

}