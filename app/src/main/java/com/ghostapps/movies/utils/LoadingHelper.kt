package com.ghostapps.movies.utils

import android.app.Activity
import android.view.View
import android.widget.RelativeLayout
import com.ghostapps.movies.R

class LoadingHelper(activity: Activity?) {

    val loadingView = activity?.findViewById<RelativeLayout>(R.id.loadingView)

    fun showProgress() {
        loadingView?.visibility = View.VISIBLE
    }

    fun hideProgress() {
        loadingView?.visibility = View.GONE
    }
}