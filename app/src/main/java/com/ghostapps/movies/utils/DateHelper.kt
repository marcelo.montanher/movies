package com.ghostapps.movies.utils

import java.text.SimpleDateFormat
import java.util.*

class DateHelper {

    private val SHORT_DATE_FORMAT = "yyyy-MM-dd"

    fun getFormatedDate(date: Date?): String {
        return SimpleDateFormat(SHORT_DATE_FORMAT, Locale.getDefault()).format(date)
    }

    fun addDays(date: Date?, days: Int): Date {
        val calendar = Calendar.getInstance()
        calendar.time = date
        calendar.add(Calendar.DAY_OF_WEEK, days)
        return calendar.time
    }

}