package com.ghostapps.movies.services.interfaces

import com.ghostapps.movies.models.MoviesList
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface MoviesSearchInterface {
    @GET("search/movie")
    fun getMovies(@Query("api_key") apiKey: String,
                  @Query("query") query: String):
            Observable<MoviesList>
}