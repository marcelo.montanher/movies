package com.ghostapps.movies.services

import android.content.Context
import com.ghostapps.movies.models.MovieModel
import com.ghostapps.movies.services.interfaces.MoviesInterface
import com.ghostapps.movies.services.interfaces.MoviesSearchInterface
import com.ghostapps.movies.utils.CacheHelper
import com.ghostapps.movies.utils.DateHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import kotlin.collections.ArrayList

class MoviesService(context: Context) {

    val apiKey = "ca3b554123994cc024b83b035057beb3"
    val minRating = "5"

    val dateHelper = DateHelper()
    val cacheHelper = CacheHelper(context)

    fun subscribe(successCallback: (moviesList: ArrayList<MovieModel>) -> Unit,
                  errorCallback: () -> Unit) {

        val retrofit = RetrofitHelper.getRetrofit().create(MoviesInterface::class.java)

        val cachedData = cacheHelper.getStoredHomeList()
        if (cachedData != null) {
            successCallback(cachedData)
        }

        val currentDate = Date()
        val previousDate = dateHelper.addDays(currentDate, -15)

        retrofit.getMovies(apiKey, dateHelper.getFormatedDate(previousDate), dateHelper.getFormatedDate(currentDate), minRating)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { moviesList ->
                            cacheHelper.saveHomeList(moviesList.results)
                            successCallback(moviesList.results)
                        },
                        { error ->
                            errorCallback() }
                )
    }

    fun subscribe(query: String,
                  successCallback: (moviesList: ArrayList<MovieModel>) -> Unit,
                  errorCallback: () -> Unit) {

        val retrofit = RetrofitHelper.getRetrofit().create(MoviesSearchInterface::class.java)

        retrofit.getMovies(apiKey, query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { moviesList ->
                            successCallback(moviesList.results)
                        },
                        { error ->
                            errorCallback() }
                )
    }

}