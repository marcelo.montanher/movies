package com.ghostapps.movies.services.interfaces

import com.ghostapps.movies.models.MoviesList
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface MoviesInterface {
    @GET("discover/movie")
    fun getMovies(@Query("api_key") apiKey: String,
                   @Query("primary_release_date.gte") fromDate: String,
                   @Query("primary_release_date.lte") toDate: String,
                   @Query("vote_average.gte") minRating: String):
            Observable<MoviesList>
}