package com.ghostapps.movies.services

import android.content.Context
import android.widget.ImageView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

class PicassoService(val context: Context?) {

    val baseUrl = "https://image.tmdb.org/t/p/w500"

    fun loadImage(url: String?, view: ImageView, onSuccess: () -> Unit, onFailure: () -> Unit) {
        Picasso.with(context).load(baseUrl + url)
                .fit()
                .centerCrop()
                .into(view, object: Callback {
                    override fun onSuccess() {
                        onSuccess()
                    }

                    override fun onError() {
                        onFailure
                    }

                })
    }
}