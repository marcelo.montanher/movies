package com.ghostapps.movies.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class MovieModel(val id: Float,
                 val vote_average: Float,
                 val backdrop_path: String?,
                 val poster_path: String?,
                 val title: String,
                 val overview: String,
                 val release_date: String,
                 val original_language: String,
                 val vote_count: Int): Parcelable