package com.ghostapps.movies.models

class MoviesList(val page: Int,
                 val total_pages: Int,
                 val results: ArrayList<MovieModel>)