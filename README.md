# movies

This project was developed in Android Studio 3.1.2.


### Tests

There's two classes in this project for tests, one for interface test (EspressoTest) and another for unit tests (UnitTest).

Location:

--app<br/>
----manifest<br/>
----java<br/>
------com.ghostapps.movies<br/>
------com.ghostapps.movies (androidTest)<br/>
--------**EspressoTest**<br/>
------com.ghostapps.movies (test)<br/>
--------**UnitTest**<br/>
----res

To run the tests you can open these files and click in the 'run' icon at the left of class's declaration.
